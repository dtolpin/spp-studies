package main

import (
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/mathx"
	"bitbucket.org/dtolpin/infergo/model"
	. "bitbucket.org/dtolpin/spp-studies/examples/ball/model/ad"
	"flag"
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"time"
)

// Command line arguments

var (
	// model
	L  = 5.
	Vw = 9.
	Vs = 11.

	SUMMARY = false

	NSTEP = 10    // thinning steps
	NITER = 10000 // iterations

	// HMC
	EPS = 0.03 // learning rate

	// sgHMC
	ETA   = EPS * EPS
	ALPHA = 0.05
	V     = 1.
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Usage = func() {
		log.Printf(`Ball throw example:
		ball [OPTIONS]` + "\n")
		flag.PrintDefaults()
	}
	flag.Float64Var(&L, "L", L, "distance to basket")
	flag.Float64Var(&Vw, "Vw", Vw, "velocity of weak throw")
	flag.Float64Var(&Vs, "Vs", Vs, "velocity of strong throw")

	flag.BoolVar(&SUMMARY, "summary", SUMMARY, "summarize posterior")

	flag.IntVar(&NSTEP, "nstep", NSTEP, "number of steps")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")

	flag.Float64Var(&EPS, "eps", EPS, "HMC learning rate")
	flag.Float64Var(&ETA, "eta", ETA, "sgHMC learning rate")
	flag.Float64Var(&ALPHA, "alpha", ALPHA, "friction")
	flag.Float64Var(&V, "V", V, "diffusion")
	log.SetFlags(0)
}

const (
	stochastic = iota + 1
	deterministic
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("'stochastic' or 'deterministic' is required")
	}
	var mode int
	switch os.Args[1] {
	case "stochastic":
		mode = stochastic
	case "deterministic":
		mode = deterministic
	default:
		log.Fatalf("expected \"stochastic\" or \"deterministic\","+
			" found %q", flag.Arg(0))
	}
	flag.CommandLine.Parse(os.Args[2:])
	if flag.NArg() > 0 {
		log.Fatalf("unexpected arguments: %v", os.Args[1])
	}

	var m model.Model
	var mcmc infer.MCMC
	switch mode {
	case stochastic:
		m = &StochasticModel{
			Vw: Vw,
			Vs: Vs,
			L:  L,
		}
		mcmc = &infer.SgHMC{
			L:     NSTEP,
			Eta:   ETA,
			Alpha: ALPHA,
			V:     V,
		}

	case deterministic:
		m = &DeterministicModel{
			Vw: Vw,
			Vs: Vs,
			L:  L,
		}
		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS,
		}

	default:
		panic("invalid mode")
	}

	x := []float64{rand.NormFloat64()}
	samples := make(chan []float64)
	mcmc.Sample(m, x, samples)

	// Burn-in
	for i := 0; i != NITER; i++ {
		<-samples
	}

	// Collect after burn-in
	s, s2, n := 0., 0., 0.
	for i := 0; i != NITER; i++ {
		x := <-samples
		if len(x) == 0 {
			break
		}
		alpha := 90 / math.Pi * math.Asin(mathx.Sigm(x[0]))
		if SUMMARY {
			s += alpha
			s2 += alpha * alpha
			n++
		} else {
			fmt.Println(alpha)
		}
	}

	mcmc.Stop()

	if SUMMARY {
		mean := s / n
		stddev := math.Sqrt(s2/n - mean*mean)
		fmt.Printf(
			"Data:\tL=%v Vw=%v Vs=%v\n"+
				"Mode:\t%s\n"+
				"Mean:\t%f\n"+
				"StdDev:\t%f\n",
			L, Vw, Vs, os.Args[1], mean, stddev)
	}
}
