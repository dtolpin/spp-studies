package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"bitbucket.org/dtolpin/infergo/mathx"
	"math/rand"
)

const (
	g = 9.80655
)

// For simplicity of the deterministic model, the player throws
// the ball with either Vw (weak throw) or Vs (strong
// throw) with equal probability.
type Model struct {
	Vw, Vs float64 // velocities of weak and strong throw
	L      float64 // distance to the basket
}

type StochasticModel Model

// The model parameter is Sigm^-1(sin(2*alpha))
func (m *StochasticModel) Observe(x []float64) float64 {
	sin2Alpha := mathx.Sigm(x[0])
	// Choose a velocity randomly.
	var v float64
	if rand.Float64() > 0.5 {
		v = m.Vs
	} else {
		v = m.Vw
	}
	d := v * v / g * sin2Alpha
	return Normal.Logp(m.L, 1, d)
}

type DeterministicModel Model

// The model parameter is Sigm^-1(sin(2*alpha))
func (m *DeterministicModel) Observe(x []float64) float64 {
	sin2Alpha := mathx.Sigm(x[0])
	// Compute weighted sum (or integral in continuous case) of
	// log-probabilities.
	dw := m.Vw * m.Vw / g * sin2Alpha
	ds := m.Vs * m.Vs / g * sin2Alpha
	return 0.5*Normal.Logp(m.L, 1, dw) + 0.5*Normal.Logp(m.L, 1, ds)
}
