package main

import (
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/model"
	. "bitbucket.org/dtolpin/spp-studies/examples/gmm/model/ad"
	smodel "bitbucket.org/dtolpin/spp-studies/model"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// Command line arguments

var (
	// model
	NCOMP = 2

	SUMMARY = false

	NSTEP = 10   // thinning steps
	NITER = 1000 // iterations
	NSAMP = 1    // gradient estimation samples

	// HMC
	EPS = 0.1 // learning rate

	// MHMC
	ETA   = EPS * EPS
	ALPHA = 0.1
	V     = 1.
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Usage = func() {
		fmt.Fprintln(flag.CommandLine.Output(),
			"Gaussian mixture model example: gmm [OPTIONS] [DATAFILE]")
		flag.PrintDefaults()
	}
	flag.IntVar(&NCOMP, "ncomp", NCOMP, "number of components")

	flag.BoolVar(&SUMMARY, "summary", SUMMARY, "summarize posterior")

	flag.IntVar(&NSTEP, "nstep", NSTEP, "number of steps")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.IntVar(&NSAMP, "nsamp", NSAMP, "number of samples per gradient")

	flag.Float64Var(&EPS, "eps", EPS, "HMC learning rate")
	flag.Float64Var(&ETA, "eta", ETA, "sgHMC learning rate")
	flag.Float64Var(&ALPHA, "alpha", ALPHA, "friction")
	flag.Float64Var(&V, "V", V, "diffusion")
	log.SetFlags(0)
}

const (
	stochastic = iota
	conditional
	deterministic
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("'stochastic', 'conditional', or 'deterministic' is required")
	}
	var mode int
	switch os.Args[1] {
	case "stochastic":
		mode = stochastic
	case "conditional":
		mode = conditional
	case "deterministic":
		mode = deterministic
	default:
		log.Fatalf("expected \"stochastic\", \"conditional\", "+
			"or \"deterministic\", found %q", os.Args[1])
	}
	flag.CommandLine.Parse(os.Args[2:])

	// Get the data
	var data []float64
	switch flag.NArg() {
	case 0:
		// Use an embedded data set, for self-check
		data = []float64{
			1.899, -1.11, -0.9068, 1.291, -0.755,
			-0.4422, -0.144, 1.214, -0.8183, -0.3386,
			0.3863, -1.036, -0.6248, 1.014, 1.336,
			-1.487, 0.8223, -0.4268, 0.6754, 0.6206,
		}
	case 1:
		// Read the CSV
		fname := flag.Arg(0)
		file, err := os.Open(fname)
		if err != nil {
			log.Fatalf("Cannot open data file %q: %v", fname, err)
		}
		rdr := csv.NewReader(file)
		for {
			record, err := rdr.Read()
			if err == io.EOF {
				break
			}
			value, err := strconv.ParseFloat(record[0], 64)
			if err != nil {
				log.Fatalf("invalid data: %v", err)
			}
			data = append(data, value)
		}
		file.Close()
	default:
		flag.Usage()
		os.Exit(1)
	}

	x := make([]float64, 2*NCOMP)
	// Set a starting  point
	if NCOMP > 1 {
		// Spread the initial components wide and thin
		for j := 0; j != NCOMP; j++ {
			x[2*j] = -2 + 4/float64(NCOMP-1)*float64(j)
		}
	}

	var (
		m    model.Model
		mcmc infer.MCMC
	)
	switch mode {
	case stochastic:
		sm := &StochasticModel{
			Data:  data,
			NComp: NCOMP,
			Assgn: make([]int, len(data)),
		}
		// Pre-assign components
		for i := range sm.Assgn {
			sm.Assgn[i] = rand.Intn(NCOMP)
		}
		sm.Observe(x)
		model.DropGradient(sm)
		for i := 0; i != NITER; i++ {
			sm.Sample(x)
		}
		m = &smodel.MarginalModel{
			SM:    sm,
			NSamp: NSAMP,
			Grad:  make([]float64, len(x)),
		}

		mcmc = &infer.SgHMC{
			L:     NSTEP,
			Eta:   ETA / float64(len(data)),
			Alpha: ALPHA,
			V:     V,
		}

	case conditional:
		sm := &StochasticModel{
			Data:  data,
			NComp: NCOMP,
			Assgn: make([]int, len(data)),
		}
		// Pre-assign components
		for i := range sm.Assgn {
			sm.Assgn[i] = rand.Intn(NCOMP)
		}
		sm.Observe(x)
		model.DropGradient(sm)
		for i := 0; i != NITER; i++ {
			sm.Sample(x)
		}
		m = sm
		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS / math.Sqrt(float64(len(data))),
		}

	case deterministic:
		m = &DeterministicModel{
			Data:  data,
			NComp: NCOMP,
		}
		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS / math.Sqrt(float64(len(data))),
		}

	default:
		panic("invalid mode")
	}

	samples := make(chan []float64)
	mcmc.Sample(m, x, samples)

	// Burn-in
	for i := 0; i != NITER; i++ {
		x = <-samples
		if len(x) == 0 {
			break
		}
		if mode == conditional {
			m.(*StochasticModel).Sample(x)
		}
	}

	// Collect after burn-in
	mean := make([]float64, NCOMP)
	std := make([]float64, NCOMP)
	meanstd := make([]float64, NCOMP)
	stdstd := make([]float64, NCOMP)
	n := 0.
	for i := 0; i != NITER; i++ {
		x := <-samples
		if len(x) == 0 {
			break
		}
		if mode == conditional {
			m.(*StochasticModel).Sample(x)
		}
		if SUMMARY {
			for j := 0; j != NCOMP; j++ {
				m, s := x[2*j], math.Exp(x[2*j+1])
				mean[j] += m
				meanstd[j] += m * m
				std[j] += s
				stdstd[j] += s * s
			}
			n++
		} else {
			for j := 0; j != NCOMP; j++ {
				m, s := x[2*j], math.Exp(x[2*j+1])
				fmt.Printf("%.4f,%.4f", m, s)
				if j == NCOMP-1 {
					fmt.Println()
				} else {
					fmt.Print(",")
				}
			}
		}
	}
	mcmc.Stop()

	if SUMMARY {
		for j := 0; j != NCOMP; j++ {
			mean[j] /= n
			meanstd[j] = math.Sqrt(meanstd[j]/n - mean[j]*mean[j])
			std[j] /= n
			stdstd[j] = math.Sqrt(stdstd[j]/n - std[j]*std[j])
		}
		fmt.Printf("Mode:\t%s\n", os.Args[1])
		fmt.Printf("Mean components:\n")
		for j := 0; j != NCOMP; j++ {
			fmt.Printf("\t%d: mean=%.4f±%.4f, std=%.4f±%.4f\n",
				j, mean[j], meanstd[j], std[j], stdstd[j])
		}
	}
}
