// Gaussian mixture
package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	ud "bitbucket.org/dtolpin/infergo/dist" // undifferentiated
	"bitbucket.org/dtolpin/infergo/mathx"
	"math"
	"math/rand"
)

type StochasticModel struct {
	Data      []float64 // samples
	NComp     int       // number of components
	Assgn     []int     // component assignments
	Mu, Sigma []float64
}

func (m *StochasticModel) Observe(x []float64) float64 {
	ll := Normal.Logps(0, 10, x...)

	if m.Mu == nil {
		m.Mu = make([]float64, m.NComp)
		m.Sigma = make([]float64, m.NComp)
	}
	for j := range m.Mu {
		m.Mu[j], m.Sigma[j] = x[2*j], math.Exp(x[2*j+1])
	}

	// Compute log likelihood of mixture
	// given the data
	for i := 0; i != len(m.Data); i++ {
		j := m.Assgn[i]
		ll += Normal.Logp(m.Mu[j], m.Sigma[j], m.Data[i])
	}

	return ll
}

// Sample morphs nuisance parameters of the model
func (m *StochasticModel) Sample(x []float64) interface{} {
	// MH pass through the assignment vector
	for i := range m.Assgn {
		j_ := m.Assgn[i]
		j := rand.Intn(m.NComp)
		if j != j_ {
			ll_ := ud.Normal.Logp(m.Mu[j_], m.Sigma[j_], m.Data[i])
			ll := ud.Normal.Logp(m.Mu[j], m.Sigma[j], m.Data[i])
			if ll-ll_ > math.Log(rand.Float64()) {
				m.Assgn[i] = j
			}
		}
	}
	return nil
}

type DeterministicModel struct {
	Data      []float64 // samples
	NComp     int       // number of components
	Mu, Sigma []float64
}

func (m *DeterministicModel) Observe(x []float64) float64 {
	ll := Normal.Logps(0, 10, x...)

	if m.Mu == nil {
		m.Mu = make([]float64, m.NComp)
		m.Sigma = make([]float64, m.NComp)
	}
	for j := range m.Mu {
		m.Mu[j], m.Sigma[j] = x[2*j], math.Exp(x[2*j+1])
	}

	// Compute log likelihood of mixture
	// given the data
	for i := 0; i != len(m.Data); i++ {
		var l float64
		for j := 0; j != m.NComp; j++ {
			lj := Normal.Logp(m.Mu[j], m.Sigma[j], m.Data[i])
			if j == 0 {
				l = lj
			} else {
				l = mathx.LogSumExp(l, lj)
			}
		}
		ll += l
	}

	return ll
}
