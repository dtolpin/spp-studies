data {
 int<lower = 0> N;
 vector[N] y;
}

parameters {
  vector[2] mu;
  real<lower=0> sigma[2];
}

model {
 mu ~ normal(0, 10);
 sigma ~ lognormal(0, 10);
 for (n in 1:N)
   target += log_mix(0.5,
                     normal_lpdf(y[n] | mu[1], sigma[1]),
                     normal_lpdf(y[n] | mu[2], sigma[2]));
}
