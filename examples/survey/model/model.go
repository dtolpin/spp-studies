package model

import (
	"bitbucket.org/dtolpin/infergo/mathx"
	"math"
	"math/rand"
)

type StochasticModel struct {
	Y    []bool
	Coin []bool
}

// Coin flip distribution
type flip struct{}

var Flip flip

func (m *flip) Observe(x []float64) float64 {
	panic("not implemented")
}
func (m *flip) Logp(p float64, x bool) float64 {
	if x {
		return math.Log(p)
	} else {
		return math.Log(1 - p)
	}
}

func (m *StochasticModel) Observe(x []float64) float64 {
	ll := 0.
	theta := mathx.Sigm(x[0])
	for i := 0; i != len(m.Y); i++ {
		if m.Coin[i] {
			ll += Flip.Logp(theta, m.Y[i])
		} else {
			ll += Flip.Logp(0.5, m.Y[i])
		}
	}
	return ll
}

// Sample morphs nuisance parameters of the model;
// returning nil hides the method from automatic differentiation
func (m *StochasticModel) Sample(x []float64) interface{} {
	// MH pass through the assignment vector
	theta := mathx.Sigm(x[0])

	// unnormalized log-likelihood of coin c given observation y
	logp := func(y, c bool) float64 {
		if c {
			if y {
				return math.Log(theta)
			} else {
				return math.Log(1 - theta)
			}
		} else {
			return math.Log(0.5)
		}
	}

	for i := range m.Coin {
		c_ := m.Coin[i]
		c := rand.Intn(2) == 1
		if c != c_ {
			ll_ := logp(m.Y[i], c_)
			ll := logp(m.Y[i], c)
			if ll-ll_ > math.Log(rand.Float64()) {
				m.Coin[i] = c
			}
		}
	}
	return nil
}

type DeterministicModel struct {
	Y []bool
}

func (m *DeterministicModel) Observe(x []float64) float64 {
	ll := 0.
	theta := mathx.Sigm(x[0])
	for i := 0; i != len(m.Y); i++ {
		ll += mathx.LogSumExp(
			Flip.Logp(theta, m.Y[i])+math.Log(0.5),
			Flip.Logp(0.5, m.Y[i])+math.Log(0.5))
	}
	return ll
}
