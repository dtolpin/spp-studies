data {
	int<lower=0> N;
	int<lower=0,upper=1> y[N];
}

parameters {
	real<lower=0,upper=1> theta;
}

model {
	for (n in 1:N) {
		target += log_mix(0.5, 
			bernoulli_lpmf(y[n]|theta),
			bernoulli_lpmf(y[n]|0.5));
	}
}
