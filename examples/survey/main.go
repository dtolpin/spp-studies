package main

import (
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/mathx"
	"bitbucket.org/dtolpin/infergo/model"
	. "bitbucket.org/dtolpin/spp-studies/examples/survey/model/ad"
	smodel "bitbucket.org/dtolpin/spp-studies/model"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// Command line arguments

var (
	// model
	THETA = 0.5 // probability of satisfaction
	N     = 20  // number of votes

	SUMMARY = false

	NSTEP = 10   // thinning steps
	NITER = 1000 // iterations
	NSAMP = 1    // gradient estimation samples

	// HMC
	EPS = 0.1 // learning rate

	// SgHMC
	ETA   = EPS * EPS
	ALPHA = 0.1
	V     = 1.
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Usage = func() {
		log.Printf(`Compensation survey example:
		survey [OPTIONS] [DATAFILE]` + "\n")
		flag.PrintDefaults()
	}
	flag.Float64Var(&THETA, "theta", THETA,
		"satisfaction probability")
	flag.IntVar(&N, "n", N, "number of votes")

	flag.BoolVar(&SUMMARY, "summary", SUMMARY, "summarize posterior")

	flag.IntVar(&NSTEP, "nstep", NSTEP, "number of steps")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.IntVar(&NSAMP, "nsamp", NSAMP, "number of samples per gradient")

	flag.Float64Var(&EPS, "eps", EPS, "HMC learning rate")
	flag.Float64Var(&ETA, "eta", ETA, "sgHMC learning rate")
	flag.Float64Var(&ALPHA, "alpha", ALPHA, "friction")
	flag.Float64Var(&V, "V", V, "diffusion")
	log.SetFlags(0)
}

const (
	stochastic = iota + 1
	conditional
	deterministic
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("'stochastic', 'conditional', or 'deterministic' is required")
	}
	var mode int
	switch os.Args[1] {
	case "stochastic":
		mode = stochastic
	case "conditional":
		mode = conditional
	case "deterministic":
		mode = deterministic
	default:
		log.Fatalf("expected \"stochastic\", \"conditional\", "+
			"or \"deterministic\", found %q", os.Args[1])
	}
	flag.CommandLine.Parse(os.Args[2:])

	var y []bool
	switch flag.NArg() {
	case 0:
		// Prepare random data
		y = make([]bool, N)
		for i := range y {
			if rand.Float64() < 0.5 {
				y[i] = rand.Float64() < THETA
			} else {
				y[i] = rand.Float64() < 0.5
			}
		}
	case 1:
		// Ignore N and theta and read the data
		// from the file
		fname := flag.Arg(0)
		file, err := os.Open(fname)
		if err != nil {
			log.Fatalf("Cannot open data file %q: %v", fname, err)
		}
		rdr := csv.NewReader(file)
		for {
			record, err := rdr.Read()
			if err == io.EOF {
				break
			}
			value, err := strconv.ParseBool(record[0])
			if err != nil {
				log.Fatalf("invalid data: %v", err)
			}
			y = append(y, value)
		}
		file.Close()
	default:
		flag.Usage()
		os.Exit(1)
	}

	// Compute actual theta from the data
	ntrue := 0
	for i := range y {
		if y[i] {
			ntrue++
		}
	}
	THETA = 2 * (float64(ntrue)/float64(len(y)) - 0.25)

	x := []float64{0.1 * rand.NormFloat64()}

	var (
		m    model.Model
		mcmc infer.MCMC
	)
	switch mode {
	case stochastic:
		sm := &StochasticModel{
			Y:    y,
			Coin: make([]bool, len(y)),
		}
		// Pre-assign components
		for i := range sm.Coin {
			sm.Coin[i] = rand.Intn(2) == 0
		}
		sm.Observe(x)
		model.DropGradient(sm)
		for i := 0; i != NITER; i++ {
			sm.Sample(x)
		}

		m = &smodel.MarginalModel{
			SM:    sm,
			NSamp: NSAMP,
			Grad:  make([]float64, len(x)),
		}
		mcmc = &infer.SgHMC{
			L:     NSTEP,
			Eta:   ETA / float64(len(y)),
			Alpha: ALPHA,
			V:     V,
		}

	case conditional:
		sm := &StochasticModel{
			Y:    y,
			Coin: make([]bool, len(y)),
		}
		// Pre-assign components
		for i := range sm.Coin {
			sm.Coin[i] = rand.Intn(2) == 0
		}
		sm.Observe(x)
		model.DropGradient(sm)
		for i := 0; i != NITER; i++ {
			sm.Sample(x)
		}
		m = sm

		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS / math.Sqrt(float64(len(y))),
		}

	case deterministic:
		m = &DeterministicModel{
			Y: y,
		}
		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS / math.Sqrt(float64(len(y))),
		}

	default:
		panic("invalid mode")
	}

	samples := make(chan []float64)
	mcmc.Sample(m, x, samples)

	// Burn-in
	for i := 0; i != NITER; i++ {
		x = <-samples
		if len(x) == 0 {
			break
		}
		if mode == conditional {
			m.(*StochasticModel).Sample(x)
		}
	}

	// Collect after burn-in
	s, s2, n := 0., 0., 0.
	for i := 0; i != NITER; i++ {
		x := <-samples
		if len(x) == 0 {
			break
		}
		if mode == conditional {
			m.(*StochasticModel).Sample(x)
		}
		theta := mathx.Sigm(x[0])
		if SUMMARY {
			s += theta
			s2 += theta * theta
			n++
		} else {
			fmt.Println(theta)
		}
	}

	mcmc.Stop()

	if SUMMARY {
		mean := s / n
		stddev := math.Sqrt(s2/n - mean*mean)
		fmt.Printf("Mode:\t%s\n", os.Args[1])
		fmt.Printf(
			"Theta:\t%f\n"+
				"Mean:\t%f\n"+
				"StdDev:\t%f\n",
			THETA, mean, stddev)
	}
}
