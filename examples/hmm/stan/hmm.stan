data {
  real<lower=0> noise;
  int<lower=1> K; // number of states
  int<lower=1> N;
  real y[N];
}

parameters {
  simplex[K] theta[K];  // transit probs
}

model {
  real acc[K];
  real gamma[N,K];
  for (k in 1:K)
    gamma[1,k] = normal_lpdf(y[0]|k-1, noise);
  for (t in 2:N) {
    for (k in 1:K) {
      for (j in 1:K)
        acc[j] = gamma[t-1,j] + log(theta[j,k]) + normal_lpdf(y[t]|k-1, noise);
      gamma[t,k] = log_sum_exp(acc);
    }
  }
  target += log_sum_exp(gamma[N]);
}
