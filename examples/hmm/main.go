package main

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/model"
	. "bitbucket.org/dtolpin/spp-studies/examples/hmm/model/ad"
	smodel "bitbucket.org/dtolpin/spp-studies/model"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// Command line arguments

var (
	// model
	NSTAT = 3  // number of hidden states
	NOISE = 0. // log stddev of observation noise

	SUMMARY = false

	NSTEP = 10   // thinning steps
	NITER = 1000 // iterations
	NSAMP = 1    // gradient estimation samples

	// HMC
	EPS = 1. // learning rate

	// sgHMC
	ETA   = EPS * EPS
	ALPHA = 0.1
	V     = 1.
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Usage = func() {
		fmt.Fprintln(flag.CommandLine.Output(),
			"Gaussian mixture model example: gmm [OPTIONS] [DATAFILE]")
		flag.PrintDefaults()
	}
	flag.IntVar(&NSTAT, "nstat", NSTAT, "number of components")
	flag.Float64Var(&NOISE, "noise", NOISE,
		"log stddev of observation noise")

	flag.BoolVar(&SUMMARY, "summary", SUMMARY, "summarize posterior")

	flag.IntVar(&NSTEP, "nstep", NSTEP, "number of steps")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.IntVar(&NSAMP, "nsamp", NSAMP,
		"number of samples per gradient")

	flag.Float64Var(&EPS, "eps", EPS, "HMC learning rate")
	flag.Float64Var(&ETA, "eta", ETA, "sgHMC learning rate")
	flag.Float64Var(&ALPHA, "alpha", ALPHA, "friction")
	flag.Float64Var(&V, "V", V, "diffusion")
	log.SetFlags(0)
}

const (
	stochastic = iota
	conditional
	deterministic
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("'stochastic', 'conditional', or 'deterministic' is required")
	}
	var mode int
	switch os.Args[1] {
	case "stochastic":
		mode = stochastic
	case "conditional":
		mode = conditional
	case "deterministic":
		mode = deterministic
	default:
		log.Fatalf("expected \"stochastic\", \"conditional\", "+
			"or \"deterministic\", found %q", os.Args[1])
	}
	flag.CommandLine.Parse(os.Args[2:])

	// Get the data
	var data []float64
	switch flag.NArg() {
	case 0:
		// Use an embedded data set, for self-check The data is
		// taken from the WebPPL HMM example, (WebPPL and
		// Anglican versions are at
		// https://bitbucket.org/probprog/anglican-white-paper)
		// with 1 added so that states start with 0 rather than
		// with -1.
		data = []float64{
			1.9, 1.8, 1.7, 1.0,
			1.025, -4.0, -1.0, 0.9,
			1.0, 1.13, 1.45, 7,
			1.2, 1.3, 0, 0,
		}
	case 1:
		// Read the CSV
		fname := flag.Arg(0)
		file, err := os.Open(fname)
		if err != nil {
			log.Fatalf("Cannot open data file %q: %v", fname, err)
		}
		rdr := csv.NewReader(file)
		for {
			record, err := rdr.Read()
			if err == io.EOF {
				break
			}
			value, err := strconv.ParseFloat(record[0], 64)
			if err != nil {
				log.Fatalf("invalid data: %v", err)
			}
			data = append(data, value)
		}
		file.Close()
	default:
		flag.Usage()
		os.Exit(1)
	}

	// The parameter vector is the transition matrix.
	x := make([]float64, NSTAT*NSTAT)
	// We start with the uniform transition matrix, with small
	// noise added. x components are on log scale and need not
	// be normalized.
	for i := range x {
		x[i] += 0.1 * rand.NormFloat64()
	}

	var (
		m    model.Model
		mcmc infer.MCMC
	)
	switch mode {
	case stochastic:
		sm := &StochasticModel{
			Data:   data,
			NStat:  NSTAT,
			Noise:  NOISE,
			States: make([]int, len(data)),
		}
		// Pre-assign states
		for i := range sm.States {
			sm.States[i] = rand.Intn(NSTAT)
		}
		sm.Observe(x)
		model.DropGradient(sm)
		for i := 0; i != NITER; i++ {
			sm.Sample(x)
		}
		m = &smodel.MarginalModel{
			SM:    sm,
			NSamp: NSAMP,
			Grad:  make([]float64, len(x)),
		}

		mcmc = &infer.SgHMC{
			L:     NSTEP,
			Eta:   ETA / float64(len(data)),
			Alpha: ALPHA,
			V:     V,
		}

	case conditional:
		sm := &StochasticModel{
			Data:   data,
			NStat:  NSTAT,
			Noise:  NOISE,
			States: make([]int, len(data)),
		}
		// Pre-assign states
		for i := range sm.States {
			sm.States[i] = rand.Intn(NSTAT)
		}
		sm.Observe(x)
		model.DropGradient(sm)
		for i := 0; i != NITER; i++ {
			sm.Sample(x)
		}
		m = sm

		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS / math.Sqrt(float64(len(data))),
		}

	case deterministic:
		m = &DeterministicModel{
			Data:  data,
			NStat: NSTAT,
			Noise: NOISE,
		}
		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS / math.Sqrt(float64(len(data))),
		}

	default:
		panic("invalid mode")
	}

	samples := make(chan []float64)
	mcmc.Sample(m, x, samples)

	// Burn-in
	for i := 0; i != NITER; i++ {
		x = <-samples
		if len(x) == 0 {
			break
		}
		if mode == conditional {
			m.(*StochasticModel).Sample(x)
		}
	}

	// Collect after burn-in
	n := 0.
	T := make([][][2]float64, NSTAT)
	for i := range T {
		T[i] = make([][2]float64, NSTAT)
	}
	t := make([]float64, NSTAT)

	// We can also collect state distributions
	// if the model is stochastic
	S := make([][]float64, len(data))
	for i := range S {
		S[i] = make([]float64, NSTAT)
	}
	for iter := 0; iter != NITER; iter++ {
		x := <-samples
		if len(x) == 0 {
			break
		}
		if mode == conditional {
			m.(*StochasticModel).Sample(x)
		}
		if SUMMARY {
			for i := 0; i != NSTAT; i++ {
				D.SoftMax(x[i*NSTAT:(i+1)*NSTAT], t)
				for j := 0; j != NSTAT; j++ {
					T[i][j][0] += t[j]
					T[i][j][1] += t[j] * t[j]
				}
			}
			switch mode {
			case conditional:
				sm := m.(*StochasticModel)
				for i := range sm.States {
					S[i][sm.States[i]]++
				}
			case stochastic:
				sm := m.(*smodel.MarginalModel).SM.(*StochasticModel)
				for i := range sm.States {
					S[i][sm.States[i]]++
				}
			}
			n++
		} else {
			// Output the transition matrix from each sample,
			// as CSV.
			for i := 0; i != NSTAT; i++ {
				D.SoftMax(x[i*NSTAT:(i+1)*NSTAT], t)
				for j := range t {
					fmt.Printf("%f", t[j])
					if i == NSTAT-1 && j == NSTAT-1 {
						fmt.Println()
					} else {
						fmt.Print(",")
					}
				}
			}
		}
	}
	mcmc.Stop()

	if SUMMARY {
		fmt.Printf("Mode:\t%s\n", os.Args[1])
		fmt.Println("Transition matrix:")
		for i := 0; i != NSTAT; i++ {
			for j := 0; j != NSTAT; j++ {
				m := T[i][j][0] / n
				s := math.Sqrt(T[i][j][1]/n - m*m)
				fmt.Printf("\t%.4f±%.4f", m, s)
			}
			fmt.Println()
		}

		switch mode {
		case conditional, stochastic:
			fmt.Println("Hidden states:")
			for i := range S {
				for j := 0; j != NSTAT; j++ {
					i, j = i, j
					p := S[i][j] / n
					fmt.Printf("\t%.4g", p)
				}
				fmt.Println()
			}
		}
	}
}
