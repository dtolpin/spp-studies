// Gaussian mixture
package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	ud "bitbucket.org/dtolpin/infergo/dist" // undifferentiated
	"math"
	"math/rand"
)

type StochasticModel struct {
	Data   []float64   // samples
	NStat  int         // number of states
	Noise  float64     // log-standard deviation of noise
	States []int       // state assignments
	logT   [][]float64 // log transition matrix
}

func (m *StochasticModel) Observe(x []float64) float64 {
	// Regularize the parameters
	ll := Normal.Logps(0, 10, x...)

	// Retrieve the transition matrix
	if m.logT == nil {
		// Allocate memory if not yet allocated
		m.logT = make([][]float64, m.NStat)
		for i := range m.logT {
			m.logT[i] = make([]float64, m.NStat)
		}
	}
	for i := range m.logT {
		D.SoftMax(x[i*m.NStat:(i+1)*m.NStat], m.logT[i])
		for j := range m.logT[i] {
			m.logT[i][j] = math.Log(m.logT[i][j])
		}
	}

	if len(m.Data) != 0 {
		noise := math.Exp(m.Noise)
		for i := range m.Data {
			// Condition states on the data
			ll += Normal.Logp(float64(m.States[i]), noise, m.Data[i])

			if i != 0 {
				// Condition states on transitions
				ll += m.logT[m.States[i-1]][m.States[i]]
			}

		}
	}

	return ll
}

// Sample morphs nuisance parameters of the model. This
// hand-coded implementation exploits model structure
// for better performance, but a general single-site
// MH would work as well even if the model is treated
// as a black box.
func (m *StochasticModel) Sample(x []float64) interface{} {
	// MH pass through the states
	noise := math.Exp(m.Noise)
	for i := range m.States {
		s_ := m.States[i]
		s := rand.Intn(m.NStat)
		if s != s_ {
			// Condition the state on observations
			ll_ := ud.Normal.Logp(float64(s_), noise, m.Data[i])
			ll := ud.Normal.Logp(float64(s), noise, m.Data[i])
			// Condition the state on transitions:
			// * from the previous state
			if i != 0 {
				ll_ += m.logT[m.States[i-1]][s_]
				ll += m.logT[m.States[i-1]][s]
			}
			// * to the next state
			if i != len(m.States)-1 {
				ll_ += m.logT[s_][m.States[i+1]]
				ll += m.logT[s][m.States[i+1]]
			}
			// MH
			if ll-ll_ > math.Log(rand.Float64()) {
				m.States[i] = s
			}
		}
	}
	return nil
}

type DeterministicModel struct {
	Data  []float64   // observations
	NStat int         // number of states
	Noise float64     // log-standard deviation of noise
	logT  [][]float64 // log transition matrix

	// buffers for forward algorithm
	acc   []float64
	gamma [][]float64
}

func (m *DeterministicModel) Observe(x []float64) float64 {
	// Regularize the parameters
	ll := Normal.Logps(0, 3, x...)

	// Retrieve the transition matrix
	if m.logT == nil {
		// Allocate memory if not yet allocated
		m.logT = make([][]float64, m.NStat)
		for i := range m.logT {
			m.logT[i] = make([]float64, m.NStat)
		}
		m.acc = make([]float64, m.NStat)
		m.gamma = make([][]float64, len(m.Data))
		for i := range m.gamma {
			m.gamma[i] = make([]float64, m.NStat)
		}
	}
	for i := range m.logT {
		D.SoftMax(x[i*m.NStat:(i+1)*m.NStat], m.logT[i])
		for j := range m.logT[i] {
			m.logT[i][j] = math.Log(m.logT[i][j])
		}
	}

	if len(m.Data) != 0 {
		noise := math.Exp(m.Noise)
		for j := 0; j != m.NStat; j++ {
		}
		// Loop over time
		for i := range m.Data {
			// Loop over current state
			for j := 0; j != m.NStat; j++ {
				m.gamma[i][j] = Normal.Logp(float64(j), noise,
					m.Data[i])
				if i != 0 {
					// Condition states on transitions
					// Loop over previous state
					for j_ := 0; j_ != m.NStat; j_++ {
						m.acc[j_] = m.gamma[i-1][j_] + m.logT[j_][j]
					}
					m.gamma[i][j] += D.LogSumExp(m.acc)
				}
			}
		}
		ll += D.LogSumExp(m.gamma[len(m.gamma)-1])
	}
	return ll
}
