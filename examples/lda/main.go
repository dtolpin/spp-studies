package main // import "bitbucket.org/dtolpin/spp-studies/examples/lda"

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"bitbucket.org/dtolpin/infergo/infer"
	"bitbucket.org/dtolpin/infergo/model"
	. "bitbucket.org/dtolpin/spp-studies/examples/lda/model/ad"
	smodel "bitbucket.org/dtolpin/spp-studies/model"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"time"
)

// Command line arguments

var (
	SUMMARY = false
	OUTPUT  = ""

	// Inference algorithm parameters
	NSTEP = 10
	NITER = 1000
	NSAMP = 1 // gradient estimation samples

	// HMC
	EPS = 0.1

	// sgHMC
	ETA   = EPS * EPS
	ALPHA = 0.1
	V     = 1.
)

func init() {
	rand.Seed(time.Now().UnixNano())
	flag.Usage = func() {
		fmt.Printf(`Gaussian mixture model: lda [OPTIONS]` + "\n")
		flag.PrintDefaults()
	}

	flag.BoolVar(&SUMMARY, "summary", SUMMARY, "summarize posterior")
	flag.StringVar(&OUTPUT, "output", OUTPUT, "output file for samples")
	flag.IntVar(&NSTEP, "nstep", NSTEP, "number of steps")
	flag.IntVar(&NITER, "niter", NITER, "number of iterations")
	flag.IntVar(&NSAMP, "nsamp", NSAMP,
		"number of samples per gradient")

	flag.Float64Var(&EPS, "eps", EPS, "HMC step")
	flag.Float64Var(&ETA, "eta", ETA, "sgHMC learning rate")
	flag.Float64Var(&ALPHA, "alpha", ALPHA, "friction")
	flag.Float64Var(&V, "V", V, "diffusion")
	log.SetFlags(0)
}

const (
	stochastic = iota
	conditional
	deterministic
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("\"stochastic\", \"conditional\", " +
			"or \"deterministic\" is required")
	}
	var mode int
	switch os.Args[1] {
	case "stochastic":
		mode = stochastic
	case "conditional":
		mode = conditional
	case "deterministic":
		mode = deterministic
	default:
		log.Fatalf("expected \"stochastic\", \"conditional\", "+
			"or \"deterministic\", found %q", os.Args[1])
	}
	flag.CommandLine.Parse(os.Args[2:])

	if flag.NArg() > 1 {
		fmt.Fprintf(os.Stderr,
			"unexpected positional arguments: %v\n",
			flag.Args()[1:])
		os.Exit(1)
	}

	outf := os.Stdout
	if OUTPUT != "" {
		_, err := os.Stat(OUTPUT)
		if !os.IsNotExist(err) {
			os.Rename(OUTPUT, OUTPUT+"~")
		}
		outf, err = os.Create(OUTPUT)
		if err != nil {
			log.Fatal(err)
		}
		defer outf.Close()
	} else {
		if SUMMARY {
			outf = nil
		}
	}

	// Get the data
	var data Data

	if flag.NArg() == 1 {
		data = Data{}
		// read the data
		fname := flag.Arg(0)
		file, err := os.Open(fname)
		if err != nil {
			fmt.Fprintf(os.Stderr,
				"Cannot open data file %q: %v\n", fname, err)
			os.Exit(1)
		}
		rdr := json.NewDecoder(file)
		err = rdr.Decode(&data)
		if err != nil {
			fmt.Fprintf(os.Stderr,
				"Error parsing data file %q: %v\n", fname, err)
			os.Exit(1)
		}
	} else {
		// use built-in data for self-testing
		data = Data{
			K: 2,
			V: 4,
			M: 5,
			N: 50,
			Word: []int{
				1, 2, 1, 2, 1, 2, 3, 2, 1, 2,
				3, 4, 3, 4, 3, 4, 3, 4, 3, 4,
				1, 2, 1, 2, 1, 2, 1, 2, 1, 2,
				3, 4, 3, 4, 3, 4, 3, 4, 3, 4,
				1, 2, 1, 3, 1, 2, 1, 2, 1, 2,
			},
			Doc: []int{
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
				3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
				4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
				5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
			},
			Alpha: []float64{0.1, 0.1},
			Beta:  []float64{0.25, 0.25, 0.25, 0.25},
		}
	}

	// Create the model and initialize the parameters
	x := make([]float64, data.M*data.K+data.K*data.V)
	for i := range x {
		x[i] = rand.NormFloat64()
	}

	var (
		m    model.Model
		mcmc infer.MCMC
	)
	switch mode {
	case stochastic:
		sm := &StochasticModel{
			Data: data,
		}
		// Pre-assign word topics
		sm.Observe(x)
		model.DropGradient(sm)
		for i := 0; i != NITER; i++ {
			sm.Sample(x)
		}
		m = &smodel.MarginalModel{
			SM:    sm,
			NSamp: NSAMP,
			Grad:  make([]float64, len(x)),
		}

		mcmc = &infer.SgHMC{
			L:     NSTEP,
			Eta:   ETA / float64(data.N),
			Alpha: ALPHA,
			V:     V,
		}

	case conditional:
		sm := &StochasticModel{
			Data: data,
		}
		// Pre-assign states
		sm.Observe(x)
		model.DropGradient(sm)
		for i := 0; i != NITER; i++ {
			sm.Sample(x)
		}
		m = sm

		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS / math.Sqrt(float64(data.N)),
		}

	case deterministic:
		m = &DeterministicModel{
			Data: data,
		}
		mcmc = &infer.HMC{
			L:   NSTEP,
			Eps: EPS / math.Sqrt(float64(data.N)),
		}

	default:
		panic("invalid mode")
	}

	samples := make(chan []float64)
	mcmc.Sample(m, x, samples)

	// Burn
	for i := 0; i != NITER; i++ {
		if len(<-samples) == 0 {
			break
		}
	}

	// Collect after burn-in
	n := 0.
	theta := make([]float64, data.K)
	phi := make([]float64, data.V)
	st := make([][][2]float64, data.M)
	for i := range st {
		st[i] = make([][2]float64, data.K)
	}
	sp := make([][][2]float64, data.K)
	for i := range sp {
		sp[i] = make([][2]float64, data.V)
	}
	for i := 0; i != NITER; i++ {
		x := <-samples
		if len(x) == 0 {
			break
		}
		for i := range st {
			D.SoftMax(model.Shift(&x, data.K), theta)
			if outf != nil {
				for j := range theta {
					fmt.Fprintf(outf, "%f,", theta[j])
				}
			}
			for j := range theta {
				st[i][j][0] += theta[j]
				st[i][j][1] += theta[j] * theta[j]
			}
		}
		for i := range sp {
			D.SoftMax(model.Shift(&x, data.V), phi)
			if outf != nil {
				for j := range phi {
					fmt.Fprintf(outf, "%f", phi[j])
					if i == data.K-1 && j == data.V-1 {
						fmt.Fprintln(outf)
					} else {
						fmt.Fprint(outf, ",")
					}
				}
			}
			for j := range phi {
				sp[i][j][0] += phi[j]
				sp[i][j][1] += phi[j] * phi[j]
			}
		}
		n++
	}
	mcmc.Stop()

	if SUMMARY {
		prSmry := func(cell [2]float64) {
			mean := cell[0] / n
			std := math.Sqrt(cell[1]/n - mean*mean)
			fmt.Printf("\t%.4f±%.4f", mean, std)
		}
		fmt.Printf("Mode:\t%s\n", os.Args[1])
		fmt.Println("Document topics:")
		for i := range st {
			for j := 0; j != data.K; j++ {
				prSmry(st[i][j])
			}
			fmt.Println()
		}
		fmt.Println("Topic words:")
		for i := range sp {
			for j := 0; j != data.V; j++ {
				prSmry(sp[i][j])
			}
			fmt.Println()
		}
	}
}
