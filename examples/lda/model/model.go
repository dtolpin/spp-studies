// LDA
package model

import (
	. "bitbucket.org/dtolpin/infergo/dist"
	"bitbucket.org/dtolpin/infergo/model"
	"math"
	"math/rand"
)

const (
	GAMMA = 10. // regularization prior
)

// data are the observations
type Data struct {
	K     int       // num topics
	V     int       // num words
	M     int       // num docs
	N     int       // total word instances
	Word  []int     // word n
	Doc   []int     // doc ID for word n
	Alpha []float64 // topic prior
	Beta  []float64 // word prior

	theta [][]float64 // topic per document probs
	phi   [][]float64 // word per topic probs
}

type StochasticModel struct {
	Data
	Topic []int
}

func (m *StochasticModel) Observe(x []float64) float64 {
	ll := 0.0

	// Regularize the parameter vector
	ll += Normal.Logps(0, GAMMA, x...)

	if m.theta == nil {
		m.theta = make([][]float64, m.M)
		for i := range m.theta {
			m.theta[i] = make([]float64, m.K)
		}
		m.phi = make([][]float64, m.K)
		for i := range m.phi {
			m.phi[i] = make([]float64, m.V)
		}
		m.Topic = make([]int, m.N)
		for i := range m.Topic {
			m.Topic[i] = rand.Intn(m.K)
		}
	}

	// Destructure parameters
	for i := range m.theta {
		D.SoftMax(model.Shift(&x, m.K), m.theta[i])
	}
	for i := range m.phi {
		D.SoftMax(model.Shift(&x, m.V), m.phi[i])
	}

	// Impose priors
	ll += Dir.Logps(m.Alpha, m.theta...)
	ll += Dir.Logps(m.Beta, m.phi...)

	// Pre-compute log theta, log phi
	for i := range m.theta {
		for j := range m.theta[i] {
			m.theta[i][j] = math.Log(m.theta[i][j])
		}
	}
	for i := range m.phi {
		for j := range m.phi[i] {
			m.phi[i][j] = math.Log(m.phi[i][j])
		}
	}

	// Condition on observations
	for i := 0; i != m.N; i++ {
		ll += m.theta[m.Doc[i]-1][m.Topic[i]] +
			m.phi[m.Topic[i]][m.Word[i]-1]
	}

	return ll
}

func (m *StochasticModel) Sample(x []float64) interface{} {
	for i := range m.Topic {
		t_ := m.Topic[i]
		t := rand.Intn(m.K)
		if t != t_ {
			ll_ := m.theta[m.Doc[i]-1][t_] + m.phi[t_][m.Word[i]-1]
			ll := m.theta[m.Doc[i]-1][t] + m.phi[t][m.Word[i]-1]
			// MH
			if ll-ll_ > math.Log(rand.Float64()) {
				m.Topic[i] = t
			}
		}
	}
	return nil
}

type DeterministicModel struct {
	Data
	gamma []float64 // buffer for marginalization
}

func (m *DeterministicModel) Observe(x []float64) float64 {
	ll := 0.0

	// Regularize the parameter vector
	ll += Normal.Logps(0, GAMMA, x...)

	if m.theta == nil {
		m.theta = make([][]float64, m.M)
		for i := range m.theta {
			m.theta[i] = make([]float64, m.K)
		}
		m.phi = make([][]float64, m.K)
		for i := range m.phi {
			m.phi[i] = make([]float64, m.V)
		}
		m.gamma = make([]float64, m.K)
	}

	// Destructure parameters
	for i := range m.theta {
		D.SoftMax(model.Shift(&x, m.K), m.theta[i])
	}
	for i := range m.phi {
		D.SoftMax(model.Shift(&x, m.V), m.phi[i])
	}

	// Impose priors
	ll += Dir.Logps(m.Alpha, m.theta...)
	ll += Dir.Logps(m.Beta, m.phi...)

	// Pre-compute log theta, log phi
	for i := range m.theta {
		for j := range m.theta[i] {
			m.theta[i][j] = math.Log(m.theta[i][j])
		}
	}
	for i := range m.phi {
		for j := range m.phi[i] {
			m.phi[i][j] = math.Log(m.phi[i][j])
		}
	}

	// Condition on observations
	for i := 0; i != m.N; i++ {
		for j := 0; j != m.K; j++ {
			m.gamma[j] = m.theta[m.Doc[i]-1][j] +
				m.phi[j][m.Word[i]-1]
		}
		ll += D.LogSumExp(m.gamma)
	}

	return ll
}
