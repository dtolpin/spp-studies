# Stochastic probabilistic programs: case studies

This repository contains case studies for paper "Stochastic probabilistic programs".

Directories: 

* `infer` --- implementations of inference algorithms.
* `examples` --- case studies from the paper:
    * `survey` --- compensation survey.
    * `ball` --- ball throw.
    * `gmm` --- Gaussian mixture model.
