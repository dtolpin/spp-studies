module bitbucket.org/dtolpin/spp-studies

go 1.13

require bitbucket.org/dtolpin/infergo v0.7.1-0.20200210091207-d6336c20fe73

replace bitbucket.org/dtolpin/infergo v0.8.0-pre => ../infergo
