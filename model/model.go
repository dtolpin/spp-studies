package model

import (
	. "bitbucket.org/dtolpin/infergo/model"
)

type StochasticModel interface {
	Model
	Sample([]float64) interface{} // hidden from autodiff
}

type MarginalModel struct {
	SM    StochasticModel
	NSamp int
	Grad  []float64
}

func (m *MarginalModel) Observe(x []float64) float64 {
	ll := 0.
	for j := range m.Grad {
		m.Grad[j] = 0.
	}
	for i := 0; i != m.NSamp; i++ {
		m.SM.Sample(x)
		l, g := m.SM.Observe(x), Gradient(m.SM)
		ll += l
		for j := range g {
			m.Grad[j] += g[j]
		}
	}
	ll /= float64(m.NSamp)
	for j := range m.Grad {
		m.Grad[j] /= float64(m.NSamp)
	}
	return ll
}

func (m *MarginalModel) Gradient() []float64 {
	return m.Grad
}
